#!/bin/bash


linhas=0
marquivo=""

if [ $(wc -l < "$1") -gt "$linhas" ]; then
	linhas=$(wc -l < "$1")
	marquivo="$1"
fi

if [ $(wc -l < "$2") -gt "$linhas" ]; then
	linhas=$(wc -l < "$2")
	marquivo="$2"
fi

if [ $(wc -l < "$3") -gt "$linhas" ]; then
	linhas=$(wc -l < "$3")
	marquivo="$3"
fi

if [ $(wc -l < "$4") -gt "$linhas" ]; then
	linhas=$(wc -l < "$4")
	marquivo="$4"
fi

echo $(cat $marquivo)
